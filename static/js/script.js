var experiments;
/**
 * String formating compared to Python and C
 * Usage: "This {0} should be replaceds".format("Wort");
 */
String.prototype.format = function() {
    var formatted = this;
    for (var arg in arguments) {
        formatted = formatted.replace("{" + arg + "}", arguments[arg]);
    }
    return formatted;
};

/**
 * Loads experiment js File
 * @param {string} id - ID of experiment
 */
function loadExperiment(id) {
    var path = experiments[id].file;
    $("#iframe").attr("src", path);
    $("#experimentTitle").html(experiments[id].title + "&nbsp;");
}

/**
 * Create dropdown menu an add listener
 * @param {string} data - JSON data string of experiment data
 */
function initDropdownMenu(data) {
    experiments = data.experiments;

    $.each(experiments, function(index, val) {
        var html = "<li><a href='#' id='dropdown_{0}'>{1}</a></li>".format(index, val.title);
        $("#myDropdown").append(html);
    });

    $("#myDropdown a").click(function(event) {
        event.preventDefault();
        var id = $(event.target).attr("id").substring(9);
        loadExperiment(id);
    });
}

/**
 * Init dropdown and load default experiumnet
 * @param {string} data - JSON data string of experiment data
 */
function initPage(data) {
    initDropdownMenu(data);
    loadExperiment(data.default);
}