/**
 * Simulation of the solar system
 * Matthias Faller
 * Source for distances: http://www.panoptikum.net/sonnensystem/
 */

/**
 * Configuration
 */
var simDay = 6; // Duration of one simulated day in seconds.
var simEarthSize = 0.2; // Diameter of earth. Base measurment in three.js units
var simEarthDistanz = 3; // Distanze earth - sun. Bas measurment in three.js

var renderer, camera, scene, controls;
var guiController = {};
var clock = new THREE.Clock();

var earth, earthClouds, moon, venus, mercury, sun, stars;
var earthOrbit = new THREE.Object3D();
var moonOrbit = new THREE.Object3D();
var venusOrbit = new THREE.Object3D();
var mercuryOrbit = new THREE.Object3D();

/**
 * log10 Polyfill
 * @param {float} x - Value
 */
Math.log10 = Math.log10 || function(x) {
  return Math.log(x) / Math.LN10;
};

/**
 * Returns calculates simulated distance.
 * @param {float} dist - Distanze to convert in kilometer
 */
function toSimDistance(distance) {
    return (distance / 150000000) * simEarthDistanz;
}

/**
 * Returns calculates simulated radius.
 * @param {float} diameter - Diameter to convert in kilometer
 */
function toSimRadius(diameter) {
    return (diameter / 12757) * simEarthSize * 0.5;
}

/**
 * Calculates rotation angle
 * @param  {number} rotationTime Duration of full rotation in earth days
 * @param  {number} time         Elapsed time in animation in seconds
 * @return {number}              Angle in radians
 */
function toSimRotation(rotationTime, time) {
    return toRad(time * 360/(simDay * rotationTime) % 360);
}

/**
 * Converts degrees to radiants
 * @param {float} deg - Degrees to convert
 */
function toRad(deg) {
    return deg * Math.PI/180;
}

function createOrbitLine(radius, segments) {
    var geometry = new THREE.Geometry();

    for (i = 0; i <= segments; i++)
    {
        var x = radius * Math.cos( i / segments * Math.PI * 2);
        var z = radius * Math.sin( i / segments * Math.PI * 2 );
        var vertex = new THREE.Vector3(x, 0, z);
        geometry.vertices.push(vertex);
    }
    material = new THREE.LineBasicMaterial( { color: 0x05205A, opacity: 0.5, linewidth: 2 } );

    var line = new THREE.Line( geometry, material );

    return line;
}

/**
 * Initialize rendere and camera
 */
function init() {
    renderer = new THREE.WebGLRenderer({
        antialias   : true,
        gammaInput  : true,
        gammaOutput : true
    });
    renderer.setClearColor( 0x000723, 1.0 );
    renderer.setSize( window.innerWidth, window.innerHeight );

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    // Slightly offset to have a more 3D View
    camera.position.set(0, 1, 1);
    // Look at the center
    camera.lookAt(new THREE.Vector3(0, 0, 0));

    controls = new THREE.OrbitControls( camera );
    controls.damping = 0.2;
    controls.maxDistance = 500;
    controls.addEventListener( 'change', render );

    // Event Listener for window resize
    $(window).resize(onWindowResize);

    createGUI();
    createScene();
}

/**
 * Event handler for window resize
 */
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}


/**
 * Creates the scene and lights
 */
function createScene() {
    scene = new THREE.Scene();

    // No simulation just to look more better.
    // Otherwise all backs of the planets would be dark
    var ambientLight = new THREE.AmbientLight( 0xD9D9D9 );
    scene.add(ambientLight);

    // Create a point light at the origin to simulate sun
    var pointLight = new THREE.PointLight( 0xFFFFFF );
    pointLight.position.set(0, 0, 0);
    scene.add(pointLight);

    createEarth();
    createMoon();
    createVenus();
    createMercury();
    createSun();
    createStars();

    // Koordinatenkreuz
    guiController.axisHelper = new THREE.AxisHelper( 0.1 );
    scene.add(guiController.axisHelper);
}

/**
 * Create planet earth ;-)
 */
function createEarth() {
    var EARTH_TILT = 23.5;
    var EARTH_RADIUS = 12742;
    var EARTH_DISTANZE = 150000000;

    var earthMap = new THREE.ImageUtils.loadTexture("static/images/earthmap1k.jpg");
    earthMap.minFilter = THREE.NearestFilter;
    var earthBump = new THREE.ImageUtils.loadTexture("static/images/earthbump1k.jpg");
    earthBump.minFilter = THREE.NearestFilter;
    var earthSpec = new THREE.ImageUtils.loadTexture('static/images/earthspec1k.jpg');
    earthSpec.minFilter = THREE.NearestFilter;

    var earthMaterial = new THREE.MeshPhongMaterial({
        map         : earthMap,
        bumpMap     : earthBump,
        bumpScale   : 0.006,
        specularMap : earthSpec,
        specular    : new THREE.Color('grey'),
        shininess   : 1
    });

    var earthGeometry = new THREE.SphereGeometry(toSimRadius(EARTH_RADIUS), 32, 32);
    earth = new THREE.Mesh(earthGeometry, earthMaterial);
    earth.position.x = toSimDistance(EARTH_DISTANZE);
    earth.rotation.z = toRad(EARTH_TILT);

    var cloudMaterial = new THREE.MeshPhongMaterial({
        color       : 0xFFFFFF,
        opacity     : 0.6,
        alphaMap    : new THREE.ImageUtils.loadTexture("static/images/earthcloudmaptrans.png"),
        transparent : true,
        shininess   : 1
    });

    var cloudGeometry = new THREE.SphereGeometry(toSimRadius(EARTH_RADIUS) * 1.01, 32, 32);
    earthClouds = new THREE.Mesh(cloudGeometry, cloudMaterial);
    earthClouds.position.copy(earth.position);

    var orbitLine = createOrbitLine(toSimDistance(EARTH_DISTANZE), 64);

    earthOrbit.add(earthClouds);
    earthOrbit.add(earth);
    earthOrbit.add(orbitLine);
    guiController.orbitLines.push(orbitLine);

    scene.add(earthOrbit);

    var vector = new THREE.Vector3();
    vector.copy(earth.position);
    camera.lookAt(vector);
}

function createMoon() {
    var moonMap = new THREE.ImageUtils.loadTexture("static/images/moonmap1k.jpg");
    moonMap.minFilter = THREE.NearestFilter;
    var moonBump = new THREE.ImageUtils.loadTexture("static/images/moonbump1k.jpg");
    moonBump.minFilter = THREE.NearestFilter;

    var moonMaterial = new THREE.MeshPhongMaterial({
        map         : moonMap,
        bumpMap     : moonBump,
        bumpScale   : 0.001,
        shininess   : 1
    });

    var moonGeometry = new THREE.SphereGeometry(toSimRadius(3476), 32, 32);
    moon = new THREE.Mesh(moonGeometry, moonMaterial);

    moonOrbit.add(moon);
    moonOrbit.position.copy(earth.position);
    scene.add(moonOrbit);

    moon.position.x = toSimDistance(384400) + 0.25; // No correct simulation
}

function createVenus() {
    var venusMap = new THREE.ImageUtils.loadTexture("static/images/venusmap.jpg");
    var venusBump = new THREE.ImageUtils.loadTexture("static/images/venusbump.jpg");

    var venusMaterial = new THREE.MeshPhongMaterial({
        map       : venusMap,
        bumpMap   : venusBump,
        bumpScale : 0.005,
        shininess : 1
    });

    var venusGeomtery = new THREE.SphereGeometry(toSimRadius(12200), 32, 32);
    venus = new THREE.Mesh(venusGeomtery, venusMaterial);
    venus.position.x = toSimDistance(108000000);

    var orbitLine = createOrbitLine(toSimDistance(108000000), 64);

    venusOrbit.add(venus);
    venusOrbit.add(orbitLine);
    guiController.orbitLines.push(orbitLine);
    scene.add(venusOrbit);
}

function createMercury() {
    var mercuryMap = new THREE.ImageUtils.loadTexture("static/images/mercurymap.jpg");

    var mercuryMaterial = new THREE.MeshPhongMaterial({
        map       : mercuryMap,
        bumpScale : 0.006,
        shininess : 1
    });

    var mercuryGeomtery = new THREE.SphereGeometry(toSimRadius(4840), 32, 32);
    mercury = new THREE.Mesh(mercuryGeomtery, mercuryMaterial);
    mercury.position.x = toSimDistance(57900000);

    var orbitLine = createOrbitLine(toSimDistance(57900000), 64);

    mercuryOrbit.add(mercury);
    mercuryOrbit.add(orbitLine);
    guiController.orbitLines.push(orbitLine);
    scene.add(mercuryOrbit);
}

function createSun() {
    var sunMap = new THREE.ImageUtils.loadTexture("static/images/sunmap.jpg");


    var sunMaterial = new THREE.MeshPhongMaterial({
        map       : sunMap,
        shininess : 1
    });

    var sunGeomtery = new THREE.SphereGeometry(toSimRadius(24000), 32, 32); // was: 1392700
    sun = new THREE.Mesh(sunGeomtery, sunMaterial);

    scene.add(sun);
}

function createStars() {
    var particleCount = 4000;
    var particles = new THREE.Geometry();
    var pMaterial = new THREE.PointCloudMaterial({
        color       : 0xFFFFFF,
        size        : 1.5,
        map         : new THREE.ImageUtils.loadTexture("static/images/starmap.png"),
        transparent : true

    });

    for (var i = 0; i < particleCount; i++) {
        var x = Math.random() * 500 - 250;
        var y = Math.random() * 500 - 250;
        var z = Math.random() * 500 - 250;
        var particle = new THREE.Vector3(x, y, z);

         particles.vertices.push(particle);
    }

    stars = new THREE.PointCloud(particles, pMaterial);
    stars.sortParticles = true;

    scene.add(stars);
}

/**
 * Update scene for each animation frame
 */
function updateScene() {
    var i;
    var time = clock.getElapsedTime();
    var vector = new THREE.Vector3();

    // Get position in world space. As eartOrbit only rotates. Position of earth will not change
    // related Object3D space of earthOrbit only related to world.
    // Help: https://stackoverflow.com/questions/15098479/how-to-get-the-global-world-position-of-a-child-object
    vector.setFromMatrixPosition(earth.matrixWorld);

    earth.rotation.y = toSimRotation(1, time);
    earthClouds.rotation.y = toSimRotation(1.2, time); // No simulation just to look more better.
    earthOrbit.rotation.y = toSimRotation(365, time);

    moon.rotation.y = toSimRotation(28, time);
    moonOrbit.position.copy(vector); // Earth position
    moonOrbit.rotation.y = toSimRotation(28, time);

    venus.rotation.y = toSimRotation(243, time);
    venusOrbit.rotation.y = toSimRotation(225, time);

    mercury.rotation.y = toSimRotation(59, time);
    mercuryOrbit.rotation.y = toSimRotation(88, time);

    stars.rotation.y += 0.00000001 % 1;
}

/**
 * GUI based on dat.gui
 */
function createGUI() {
    var gui = new dat.GUI();

    guiController.simDay =  simDay;
    guiController.orbitLines = [];
    guiController.showOrbit =  true;
    guiController.axisHelper = null;
    guiController.showAxisHelper = true;

    var simDayControler = gui.add(guiController, "simDay", 1, 20).name("Day in s");
    var showOrbitControler = gui.add(guiController, "showOrbit").name("Show Orbit");
    var showAxisHelper = gui.add(guiController, "showAxisHelper").name("Show Axis");

    simDayControler.onChange(function() {
        simDay = guiController.simDay;
    });

    showOrbitControler.onChange(function() {
        for (var i = 0; i < guiController.orbitLines.length; i++) {
            guiController.orbitLines[i].visible = guiController.showOrbit;
        }
    });

    showAxisHelper.onChange(function(){
        guiController.axisHelper.visible = guiController.showAxisHelper ;
    });
}

/**
 * Add render result to DOM. Avoids duplication of elements if
 * same script will be reloaded
 */
function addToDOM() {
    if ($("#container").find("canvas").length > 0) {
        $("#container").remove("canvas");
    }
    $("#container").append(renderer.domElement);
}

/**
 * Update and render scene
 */
function render() {
    updateScene();
    renderer.render(scene, camera);
}

/**
 * Request animation frame and render
 */
function animate() {
    window.requestAnimationFrame(animate);
    render();
}

/**
 * Try to set up canvas an draw init and load WebGL Content after page is loaded
 */
$(document).ready(function() {
    try {
        init();
        addToDOM();
        animate();
    } catch(e) {
        var errorReport = "Your program encountered an unrecoverable error, can not draw on canvas. Error was:<br/><br/>";
        $('#container').append(errorReport+e);
    }
});