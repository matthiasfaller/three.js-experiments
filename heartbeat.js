/**
 * Simulation of heartbeat
 * Matthias Faller
 */

var renderer, camera, scene, controls, light;
var guiController = {};
var clock = new THREE.Clock();

var heart;

var shaderUniforms = {
    amplitude: {
        type: 'f', // a float
        value: 0
    }
};

/**
 * Converts degrees to radiants
 * @param {float} deg - Degrees to convert
 */
function toRad(deg) {
    return deg * Math.PI/180;
}

/**
 * Initialize rendere and camera
 */
function init() {
    renderer = new THREE.WebGLRenderer({
        antialias   : true,
        gammaInput  : true,
        gammaOutput : true
    });
    renderer.setClearColor( 0x000723, 1.0 );
    renderer.setSize( window.innerWidth, window.innerHeight );

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    // Slightly offset to have a more 3D View
    camera.position.set(0, 0, 3);
    // Look at the center
    camera.lookAt(new THREE.Vector3(0, 0, 0));


    controls = new THREE.OrbitControls( camera );
    controls.damping = 0.2;
    controls.maxDistance = 500;
    controls.addEventListener( 'change', render );

    // Event Listener for window resize
    $(window).resize(onWindowResize);

    //createGUI();
    //createScene();
    var url = "static/models/Heart.dae";
    var loader = new THREE.ColladaLoader();
    loader.options.convertUpAxis = true;
    loader.load(url, createScene);
}

function createScene(collada) {
    scene = new THREE.Scene();

        scene.add( camera );

    var ambientLight = new THREE.AmbientLight( 0x3F3F3F );
    scene.add(ambientLight);

    var pointLight = new THREE.PointLight( 0xFFFFFF );
    pointLight.position.set(2, 2, 2);
    scene.add(pointLight);

    heart = new THREE.Object3D();
    heart = collada.scene.children[2].children[0].clone();



    heart.material = new THREE.ShaderMaterial({
        uniforms       : shaderUniforms,
        vertexShader   : document.getElementById( 'vertexShader' ).textContent,
        fragmentShader : document.getElementById( 'fragmentShader' ).textContent,
    });

    heart.scale.set(0.1, 0.1, 0.1);
    heart.rotation.set(toRad(90), 0, toRad(90));
    heart.rotation.z = toRad(90);

    scene.add(heart);

    scene.add(new THREE.AxisHelper( 1 ));

    animate();
}

/**
 * Event handler for window resize
 */
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

/**
 * Add render result to DOM. Avoids duplication of elements if
 * same script will be reloaded
 */
function addToDOM() {
    if ($("#container").find("canvas").length > 0) {
        $("#container").remove("canvas");
    }
    $("#container").append(renderer.domElement);
}

function updateScene() {
    var time = clock.getElapsedTime();

    shaderUniforms.amplitude.value = Math.sin(15 * time) * 0.5;
}

/**
 * Update and render scene
 */
function render() {
    updateScene();
    renderer.render(scene, camera);
}

/**
 * Request animation frame and render
 */
function animate() {
    window.requestAnimationFrame(animate);
    render();
}

/**
 * Try to set up canvas an draw init and load WebGL Content after page is loaded
 */
$(document).ready(function() {
    try {
        init();
        addToDOM();
    } catch(e) {
        var errorReport = "Your program encountered an unrecoverable error, can not draw on canvas. Error was:<br/><br/>";
        $('#container').append(errorReport+e);
    }
});